.PHONY: all test build clean

# Load environment variables from .env file
include .env
export $(shell sed 's/=.*//' .env)

all: build

# Build the Go project
build:
	go build -o myapp ./...

# Run tests with .env variables loaded and disable caching
test:
	go test -v -count=1 ./...

# Clean the build output
clean:
	go clean
	rm -f myapp
